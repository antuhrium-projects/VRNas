"use client";

import { useState } from "react";
import Link from "next/link";
import { usePathname } from "next/navigation";

import DropButton from "./ui/buttons/DropButton";

const NavLinks = ({ links }) => {
  const pathName = usePathname();
  const [isOpened, setIsOpened] = useState("");

  return (
    <div className="hidden lg:flex lg:items-center lg:gap-16 ">
      {links.map((link) =>
        link.links.length === 0 ? (
          <Link
            key={link.name}
            className={`hover:text-white text-lg ${
              pathName.slice(1) === link.url && "text-white font-medium"
            }`}
            href={link.url}
          >
            {link.name}
          </Link>
        ) : (
          <DropButton
            key={link.name}
            title={link.name}
            links={link.links}
            isOpened={isOpened}
            setIsOpened={setIsOpened}
          />
        )
      )}
    </div>
  );
};

export default NavLinks;
