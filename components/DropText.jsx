"use client";

import { useState } from "react";
import { motion } from "framer-motion";

import ArrowButton from "./ui/buttons/ArrowButton";
import { VDropDown } from "@/variants";

const DropText = () => {
  const [drop, setDrop] = useState(false);

  return (
    <div className="flex flex-col gap-3">
      <button onClick={() => setDrop((prev) => !prev)}>
        <div className="border-b pb-3 border-[--gray] flex justify-between items-center">
          <h4 className="text-medium text-xl">
            Passionate and Experienced Team
          </h4>
          <ArrowButton drop={drop} />
        </div>
      </button>

      {drop && (
        <motion.p
          variants={VDropDown}
          initial="closed"
          animate="opened"
          exit="closed"
          className={`section-text ${
            drop ? "block" : "hidden"
          } overflow-y-hidden`}
        >
          We are proud of our team of VR experts who are passionate about VR and
          dedicated to delivering the highest quality work. Our team consists of
          experienced VR developers, designers, and technicians who have a
          proven track record of creating immersive and engaging VR experiences.
        </motion.p>
      )}
    </div>
  );
};

export default DropText;
