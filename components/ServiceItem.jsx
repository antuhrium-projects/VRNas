"use client";

import { useState } from "react";
import { motion } from "framer-motion";
import Image from "next/image";

import SecButton from "@/components/ui/buttons/SecButton";
import { VFromBottom, transitionFast } from "@/variants";

const ServiceItem = ({ service }) => {
  const [hover, setHover] = useState(false);

  return (
    <motion.div
      variants={VFromBottom}
      initial="initial"
      whileInView="final"
      exit="initial"
      onHoverStart={() => {
        setHover(true);
      }}
      onHoverEnd={() => {
        setHover(false);
      }}
      key={service.text}
      className={`relative lg:max-w-[380px] h-[290px] p-8 bg-[--main-dark-color] border border-gray-500 flex flex-col gap-3 ${
        hover &&
        "after:content-[''] after:block after:w-full after:h-2/3 after:absolute after:bottom-0 after:left-0 after:bg-gradient-to-t after:from-[--main-dark-color] after:to-0 z-10"
      }`}
    >
      <Image src={service.image} width={48} height={48} alt="icon" />
      <h3 className="mt-12">{service.title}</h3>
      <p className="section-text">{service.text}</p>
      <SecButton
        transition={transitionFast}
        initial={{ opacity: 0, y: "50%", x: "50%" }}
        animate={hover ? { opacity: 1, y: 0, x: "50%" } : ""}
        exit={{ opacity: 0, y: "50%", x: "50%" }}
        className={`absolute bottom-6 right-1/2 translate-x-1/2 bg-[--main-dark-color]`}
      >
        LEARN MORE
      </SecButton>
    </motion.div>
  );
};

export default ServiceItem;
