"use client";
import { useState } from "react";

import { FaRegCirclePlay } from "react-icons/fa6";

const VideoPlayer = ({ w, h }) => {
  const [play, setPlay] = useState(false);

  return (
    <>
      {!play ? (
        <button
          onClick={() => setPlay(true)}
          className="transition-all hover:scale-110"
        >
          <FaRegCirclePlay className="text-4xl z-30 transition-all duration-200" />
        </button>
      ) : (
        <iframe
          width={w}
          height={h}
          src="https://www.youtube.com/embed/FYH9n37B7Yw?autoplay=1&controls=0"
          title="YouTube video"
          allow="autoplay"
        ></iframe>
      )}
    </>
  );
};

export default VideoPlayer;
