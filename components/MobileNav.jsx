"use client";
import Link from "next/link";
import React, { useState } from "react";
import { AnimatePresence, motion } from "framer-motion";

import DropButton from "./ui/buttons/DropButton";
import SecButton from "./ui/buttons/SecButton";
import { VFromLeft, transitionFast } from "@/variants";

const MobileNav = ({ openNav, links }) => {
  const [isOpened, setIsOpened] = useState("");
  return (
    <motion.nav
      transition={transitionFast}
      variants={VFromLeft}
      initial="initial"
      animate="final"
      exit="initial"
      className="absolute top-0 left-0 w-2/3 h-screen z-20 flex flex-col gap-5 pt-32 px-5 bg-[--main-dark-color]"
    >
      {links.map((link) =>
        link.links.length === 0 ? (
          <Link key={link.name} href={link.url} className="text-3xl">
            {link.name}
          </Link>
        ) : (
          <DropButton
            openNav={openNav}
            key={link.name}
            title={link.name}
            links={link.links}
            isOpened={isOpened}
            setIsOpened={setIsOpened}
          />
        )
      )}
      <SecButton>Contact us</SecButton>
    </motion.nav>
  );
};

export default MobileNav;
