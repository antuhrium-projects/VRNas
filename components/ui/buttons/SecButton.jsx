"use client";
import { motion } from "framer-motion";

const SecButton = ({ children, className, ...rest }) => {
  return (
    <motion.button {...rest} className={`sec-button text-lg ${className}`}>
      {children}
    </motion.button>
  );
};

export default SecButton;
