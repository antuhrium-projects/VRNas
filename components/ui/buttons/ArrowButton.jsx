import { motion } from "framer-motion";

const ArrowButton = ({ color = "white", drop }) => {
  return (
    <motion.svg
      animate={drop ? { rotate: "180deg" } : ""}
      width="7"
      height="4"
      viewBox="0 0 7 4"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M3.0166 3.68789L0.0166016 0.687891L0.554101 0.150391L3.0166 2.62539L5.4791 0.16289L6.0166 0.70039L3.0166 3.68789Z"
        fill={color}
      />
    </motion.svg>
  );
};

export default ArrowButton;
