"use client";

import { motion } from "framer-motion";
import { IoIosArrowDown } from "react-icons/io";
import Link from "next/link";

import { VDropDown, transitionFast } from "@/variants";

const DropButton = ({ title, links, isOpened, setIsOpened }) => {
  const PageClick = () => {
    setIsOpened(isOpened === title ? "" : title);
  };

  return (
    <motion.div
      className="relative py-2"
      onHoverStart={() => setIsOpened(title)}
      onHoverEnd={() => setIsOpened("")}
    >
      <button
        href="#"
        className="flex items-center gap-1 z-10 text-3xl lg:text-lg"
        onClick={PageClick}
      >
        {title}
        <motion.div
          transition={transitionFast}
          animate={isOpened === title ? { rotate: "180deg" } : {}}
        >
          <IoIosArrowDown className="text-[#D1D1D1]" />
        </motion.div>
      </button>
      {isOpened === title && (
        <motion.div
          transition={transitionFast}
          variants={VDropDown}
          initial="closed"
          animate="opened"
          exit="closed"
          className="absolute top-10 left-0 z-30 w-max p-6 bg-[--dark-color] lg:bg-[--main-dark-color] flex flex-col gap-2 overflow-y-hidden rounded-md"
        >
          {isOpened
            ? links.map((link) => (
                <Link
                  key={link.name}
                  href={link.url}
                  className={`text-lg font-light`}
                >
                  {link.name}
                </Link>
              ))
            : ""}
        </motion.div>
      )}
    </motion.div>
  );
};

export default DropButton;
