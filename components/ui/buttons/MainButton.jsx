import { motion } from "framer-motion";

const MainButton = ({ children, className, ...rest }) => {
  return (
    <motion.button
      {...rest}
      className={`gradient flex items-center justify-center px-6 py-3 text-sm font-medium uppercase transition-transform hover:scale-110 ${className}`}
    >
      {children}
    </motion.button>
  );
};

export default MainButton;
