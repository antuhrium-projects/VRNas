import { Chakra_Petch } from "next/font/google";
import "./globals.css";

const chakra = Chakra_Petch({ subsets: ["latin"], weight: "400" });

export const metadata = {
  title: "VRNas | Immerse Yourself in Virtual Reality",
  description:
    "Experience Unforgettable Events in VR. Bring your events to life like never before with our VR services",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={"bg-[--dark-color] overflow-x-hidden text-white"}>
        {children}
      </body>
    </html>
  );
}
