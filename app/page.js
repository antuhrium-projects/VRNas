"use client";

import Header from "@/sections/Header";
import AboutSection from "@/sections/AboutSection";
import Footer from "@/sections/Footer";
import GetStartSection from "@/sections/GetStartSection";
import MainSection from "@/sections/MainSection";
import ServiceSection from "@/sections/ServiceSection";
import SubscribeSection from "@/sections/SubscribeSection";
import WhyChooseSection from "@/sections/WhyChooseSection";

import { MotionConfig, AnimatePresence } from "framer-motion";
import { transitionMedium } from "@/variants";

const HomePage = () => {
  return (
    <AnimatePresence initial={true} mode="wait">
      <MotionConfig transition={transitionMedium}>
        <main className="w-screen">
          <Header />
          <MainSection />
          <AboutSection />
          <ServiceSection />
          <WhyChooseSection />
          <GetStartSection />
          <SubscribeSection />
          <Footer />
        </main>
      </MotionConfig>
    </AnimatePresence>
  );
};

export default HomePage;
