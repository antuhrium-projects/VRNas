export const transitionSlow = {
  duration: 1,
  ease: "easeInOut",
};

export const transitionMedium = {
  duration: 0.8,
  ease: "easeInOut",
};

export const transitionFast = {
  duration: 0.2,
  ease: "easeInOut",
};

export const VDropDown = {
  closed: {
    height: 0,
    opacity: 0,
  },
  opened: {
    height: "fit-content",
    opacity: 1,
  },
};

export const VFromRight = {
  initial: {
    x: "100%",
    opacity: 0,
  },
  final: {
    x: 0,
    opacity: 1,
  },
};

export const VFromLeft = {
  initial: {
    x: "-100%",
    opacity: 0,
  },
  final: {
    x: 0,
    opacity: 1,
  },
};

export const VFromBottom = {
  initial: {
    y: "50px",
    opacity: 0,
  },
  final: {
    y: 0,
    opacity: 1,
  },
};
