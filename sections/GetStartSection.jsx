import { motion } from "framer-motion";

import MainButton from "@/components/ui/buttons/MainButton";
import { VFromBottom, transitionFast } from "@/variants";

const GetStartSection = () => {
  return (
    <section className="container px-5 mt-[100px] md:mt-[150px] flex flex-col items-center justify-center gap-12">
      <motion.div
        variants={VFromBottom}
        initial="initial"
        whileInView="final"
        exit="initial"
        className="flex flex-col items-center justify-center gap-3 max-w-3xl"
      >
        <h4 className="section-name text-center">HOW TO GET STARTED</h4>
        <h2 className="section-title text-center">
          Bringing Your Virtual Reality Dreams to Life
        </h2>
      </motion.div>
      <motion.iframe
        variants={VFromBottom}
        initial="initial"
        whileInView="final"
        exit="initial"
        className=" rounded-xl w-full h-[254px] md:h-[542px] lg:h-[675px]"
        width={1200}
        height={675}
        src="https://www.youtube.com/embed/eR5vsN1Lq4E?controls=0"
        title="YouTube video"
        allowFullScreen={true}
      ></motion.iframe>
      <MainButton
        transition={transitionFast}
        variants={VFromBottom}
        initial="initial"
        whileInView="final"
        exit="initial"
      >
        Get Started
      </MainButton>
    </section>
  );
};

export default GetStartSection;
