import VideoPlayer from "@/components/VideoPlayer";
import MainButton from "@/components/ui/buttons/MainButton";
import Image from "next/image";

import { motion } from "framer-motion";
import { VFromBottom, VFromLeft, transitionSlow } from "@/variants";

const AboutSection = () => {
  return (
    <section className="container relative p-5 lg:p-0 flex flex-col md:flex-row md:items-center lg:items-end overflow-x-hidden md:overflow-visible">
      {/* left image */}
      <div className="relative lg:w-1/2 flex justify-center items-center">
        {/* image */}

        <motion.div
          transition={transitionSlow}
          variants={VFromLeft}
          initial="initial"
          whileInView="final"
          exit="initial"
          className="mt-[120px] ml-5"
        >
          <Image src={"/AboutImg.png"} width={520} height={620} alt="about" />
        </motion.div>

        {/* bg */}
        <motion.div
          variants={VFromBottom}
          initial="initial"
          whileInView="final"
          exit="initial"
          className="bg-[--main-dark-color] absolute bottom-0 md:left-0 w-[350px] h-[500px] -z-20"
        />
        <motion.div
          variants={VFromBottom}
          initial="initial"
          whileInView="final"
          exit="initial"
          className="w-[600px] h-[200px] absolute bottom-[100px] left-[-60px] -z-10 gradient blur-3xl opacity-60 rotate-[-45deg]"
        />

        {/* video */}

        <motion.div
          variants={VFromBottom}
          initial="initial"
          whileInView="final"
          exit="initial"
          className="absolute bottom-[-1.3rem] right-6 md:right-0 lg:right-10 w-[196px] h-[128px] flex items-center justify-center bg-[--main-dark-color]"
        >
          <VideoPlayer w={196} h={128} />
        </motion.div>
      </div>
      {/* right text */}
      <motion.div
        variants={VFromBottom}
        initial="initial"
        whileInView="final"
        exit="initial"
        className="md:w-2/3 mt-16 md:ml-20 md:mb-10 flex flex-col justify-center gap-3"
      >
        <h4 className="section-name">ABOUT US</h4>
        <h2 className="max-w-[550px] section-title">
          Bring your events to life like never before with our VR services.
        </h2>
        <p className="max-w-[530px] section-text">
          VRNas is a leading provider of VR services for education,
          entertainment, architecture, and events. Our mission is to bring the
          power of virtual reality to everyone, allowing them to explore new
          worlds, learn in new ways, and experience events in a whole new light.
        </p>
        <ul className="mt-3 flex flex-col gap-3">
          <li className="flex gap-3 items-center font-normal text-[1rem]">
            <Image
              src={"/AboutCheckBox.png"}
              width={16}
              height={16}
              alt="dignity"
            />
            Cutting-Edge Technology
          </li>
          <li className="flex gap-3 items-center font-normal text-[1rem]">
            <Image
              src={"/AboutCheckBox.png"}
              width={16}
              height={16}
              alt="dignity"
            />
            Versatile Applications
          </li>
          <li className="flex gap-3 items-center font-normal text-[1rem]">
            <Image
              src={"/AboutCheckBox.png"}
              width={16}
              height={16}
              alt="dignity"
            />
            Affordable and Accessible
          </li>
        </ul>
        <MainButton className={"mt-5 w-fit"}>Read more</MainButton>
      </motion.div>
    </section>
  );
};

export default AboutSection;
