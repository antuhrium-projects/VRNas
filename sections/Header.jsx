"use client";
import { useState } from "react";
import Image from "next/image";

import { AiOutlineMenu } from "react-icons/ai";
import { RxCross2 } from "react-icons/rx";

import SecButton from "../components/ui/buttons/SecButton";
import NavLinks from "../components/NavLinks";
import MobileNav from "@/components/MobileNav";

const links = [
  { name: "Home", url: "", links: [] },
  { name: "About us", url: "#", links: [] },
  { name: "Service", url: "#", links: [] },
  {
    name: "Page",
    links: [
      {
        name: "Pricing Plan",
        url: "#",
      },
      {
        name: "FAQ",
        url: "#",
      },
      {
        name: "Privacy Policy",
        url: "#",
      },
    ],
  },
  {
    name: "Blog",
    links: [
      {
        name: "Resent Articles",
        url: "#",
      },
      {
        name: "Our Team",
        url: "#",
      },
      {
        name: "Detail Service",
        url: "#",
      },
    ],
  },
];

const Header = () => {
  const [openNav, setOpenNav] = useState(false);

  return (
    <header className="bg-gradient-to-b from-[--dark-color] to-transparent w-screen relative py-5">
      {openNav && <MobileNav openNav={openNav} links={links} />}
      <div className="lg:container flex justify-between w-screen px-5 lg:px-0">
        <Image
          src="./logo.svg"
          width={100}
          height={30}
          alt="VRNas's logo"
          className="z-40"
        />
        <NavLinks links={links} />
        <SecButton className={"hidden lg:flex"}>Contact us</SecButton>

        <div className="flex items-center gap-10 lg:hidden z-40">
          {openNav ? (
            <button
              className="text-3xl right-5 top-8"
              onClick={() => setOpenNav((prev) => !prev)}
            >
              <RxCross2 />
            </button>
          ) : (
            <button
              className="text-3xl"
              onClick={() => setOpenNav((prev) => !prev)}
            >
              <AiOutlineMenu />
            </button>
          )}
        </div>
      </div>
    </header>
  );
};

export default Header;
