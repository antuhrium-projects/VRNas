import Image from "next/image";
import { motion } from "framer-motion";

import { AiOutlineInstagram } from "react-icons/ai";
import { BiLogoFacebook } from "react-icons/bi";
import { AiFillGithub } from "react-icons/ai";

import { GoLocation } from "react-icons/go";
import { LuMail } from "react-icons/lu";
import { FiPhoneCall } from "react-icons/fi";
import { VFromBottom, transitionSlow } from "@/variants";

const Quicklinks = ["Home", "Pricing Plan", "Service", "Blog", "Our Team"];
const Support = [
  "About us",
  "Contact us",
  "FAQ",
  "Tems & Conditions",
  "Privacy Policy",
];

const Footer = () => {
  return (
    <motion.footer
      transition={transitionSlow}
      variants={VFromBottom}
      initial="initial"
      whileInView="final"
      exit="initial"
      className="mt-[100px] md:mt-[150px] relative"
    >
      <div className="after:bg-[url(/bg/striped.png)] after:content-[''] after:absolute after:bottom-0 after:left-0 after:w-full after:md:w-[730px] after:h-[350px] after:bg-contain after:bg-no-repeat after:rotate-180" />
      <div className="container px-5 flex flex-wrap">
        <div className="flex lg:flex-col items-center lg:items-start justify-between lg:justify-between w-full lg:w-1/3">
          <Image src="./logo.svg" width={100} height={30} alt="VRNas's logo" />
          <div className="flex items-center gap-5">
            <div className="socials">
              <AiOutlineInstagram className="text-3xl" />
            </div>
            <div className="socials">
              <BiLogoFacebook className="text-3xl" />
            </div>
            <div className="socials">
              <AiFillGithub className="text-3xl" />
            </div>
          </div>
        </div>

        <div className="flex flex-wrap gap-[100px] mt-14 lg:mt-0">
          <div className="flex flex-col gap-4">
            <span className="font-semibold text-lg">Quicklinks</span>
            {Quicklinks.map((item) => (
              <span key={item} className="font-normal text-base cursor-pointer">
                {item}
              </span>
            ))}
          </div>
          <div className="flex flex-col gap-4">
            <span className="font-semibold text-lg">Support</span>
            {Support.map((item) => (
              <span key={item} className="font-normal text-base cursor-pointer">
                {item}
              </span>
            ))}
          </div>
          <div className="flex flex-col gap-4">
            <span className="font-semibold text-lg">Need Help?</span>
            <span className="font-normal text-base flex items-center gap-2">
              <GoLocation className="text-xl" />
              Tanjung Sari Street no.48, Pontianak City
            </span>
            <span className="font-normal text-base flex items-center gap-2">
              <LuMail className="text-xl" />
              Support@VRNas.com
            </span>
            <span className="font-normal text-base flex items-center gap-2">
              <FiPhoneCall className="text-xl" />
              +123 456 7890
            </span>
          </div>
        </div>

        <div className="w-full gradient mt-16">
          <div className="flex justify-center py-8 bg-[--dark-color] mt-px font-light">
            © Copyright 2024, All Rights Reserved
          </div>
        </div>
      </div>
    </motion.footer>
  );
};

export default Footer;
