import { motion } from "framer-motion";

import ServiceItem from "@/components/ServiceItem";
import { VFromBottom } from "@/variants";

const services = {
  group1: [
    {
      title: "VR Development",
      text: "From concept to creation, our team of VR developers will bring your vision to life.",
      image: "/icons/serviceIcons/1.svg",
    },
    {
      title: "VR Design",
      text: "Our talented VR designers will create immersive and engaging environments that will captivate your audience.",
      image: "/icons/serviceIcons/2.svg",
    },
  ],
  group2: [
    {
      title: "VR Consulting",
      text: "Our VR consultants will work with you to ensure that your VR experience meets your goals and exceeds your expectations.",
      image: "/icons/serviceIcons/3.svg",
    },
    {
      title: "VR Games",
      text: "We offer a wide selection of VR games that are suitable for players of all ages and skill levels.",
      image: "/icons/serviceIcons/4.svg",
    },
  ],
  group3: [
    {
      title: "VR Events",
      text: "Make your next event unforgettable with our VR event services.",
      image: "/icons/serviceIcons/5.svg",
    },
    {
      title: "VR Entertainment",
      text: "Create a VR escape room, or offer VR experiences at a theme park, we have the expertise and experience to make it happen.",
      image: "/icons/serviceIcons/6.svg",
    },
  ],
};
const ServiceSection = () => {
  return (
    <motion.section
      variants={VFromBottom}
      initial="initial"
      whileInView="final"
      exit="initial"
      className="container px-5 mt-[150px] flex flex-col gap-6"
    >
      {/* Top Text */}
      <div className="flex flex-col lg:flex-row lg:justify-between lg:items-center gap-3 ">
        <div className="flex flex-col gap-3">
          <h4 className="section-name">OUR SERVICE</h4>
          <h2 className="max-w-[550px] section-title">Our Service</h2>
        </div>
        <p className="section-text lg:w-1/2">
          We use the latest VR hardware and software to create high-quality VR
          experiences that are accessible and affordable. Our goal is to provide
          exceptional customer service and support, and our team is always
          available to answer any questions and address any concerns you may
          have.
        </p>
      </div>

      {/* Services */}
      <div className="flex flex-col lg:flex-row gap-8 md:mt-20">
        <div className="flex flex-col md:flex-row lg:flex-col gap-8">
          {services.group1.map((service) => (
            <ServiceItem key={service.title} service={service} />
          ))}
        </div>
        <div className="lg:mt-14 flex flex-col md:flex-row lg:flex-col gap-8">
          {services.group3.map((service) => (
            <ServiceItem key={service.title} service={service} />
          ))}
        </div>
        <div className="lg:mt-32 flex flex-col md:flex-row lg:flex-col gap-8">
          {services.group2.map((service) => (
            <ServiceItem key={service.title} service={service} />
          ))}
        </div>
      </div>
    </motion.section>
  );
};

export default ServiceSection;
