import VideoPlayer from "@/components/VideoPlayer";
import MainButton from "@/components/ui/buttons/MainButton";
import Image from "next/image";

import { motion } from "framer-motion";
import { VFromBottom, VFromLeft, VFromRight, transitionSlow } from "@/variants";

const MainSection = () => {
  return (
    <section className="lg:container lg:mx-auto mt-10 overflow-x-hidden md:overflow-x-visible h-fit">
      {/* main */}
      <div className="flex justify-between flex-col md:flex-row">
        {/* left text + under text */}
        <div className="px-5">
          {/* left text */}
          <div className="lg:mt-[74px]">
            <motion.h1
              variants={VFromBottom}
              initial="initial"
              whileInView="final"
              exit="initial"
              className="lg:text-6xl mt-20 lg:mt-10 text-5xl font-semibold max-w-[500px]"
            >
              Immerse Yourself in Virtual Reality
            </motion.h1>
            <motion.p
              variants={VFromBottom}
              initial="initial"
              whileInView="final"
              exit="initial"
              className="mt-[12px] max-w-md"
            >
              Experience Unforgettable Events in VR. Bring your events to life
              like never before with our VR services
            </motion.p>
            <MainButton
              variants={VFromLeft}
              initial="initial"
              whileInView="final"
              exit="initial"
              className="mt-[32px]"
            >
              discover more
            </MainButton>
          </div>

          {/* under text */}
          <div className="flex flex-col mt-8 lg:flex-row lg:items-center gap-[74px] md:mb-9">
            {/* photos under text */}
            <motion.div
              variants={VFromLeft}
              initial="initial"
              whileInView="final"
              exit="initial"
              className="flex flex-col md:flex-row md:items-center gap-5"
            >
              <div className="flex">
                <Image
                  src={"/people/1.png"}
                  width={50}
                  height={50}
                  alt="client"
                />
                <Image
                  className="ml-[-12px]"
                  src={"/people/2.png"}
                  width={50}
                  height={50}
                  alt="client"
                />
                <Image
                  className="ml-[-12px]"
                  src={"/people/3.png"}
                  width={50}
                  height={50}
                  alt="client"
                />
              </div>
              <span
                className="font-light relative after:content-[''] after:absolute after:top-[18px]
                after:left-[-6px] after:w-[50px] after:h-[6px] after:bg-[url('/underline.svg')]"
              >
                <span className="font-bold">32k+ </span>
                Happy Client
              </span>
            </motion.div>
            {/* video under text */}
            <motion.div
              variants={VFromBottom}
              initial="initial"
              whileInView="final"
              exit="initial"
              className="w-[196px] h-[128px] flex items-center justify-center bg-[--main-dark-color]"
            >
              <VideoPlayer w={196} h={128} />
            </motion.div>
          </div>
        </div>

        {/* right image + bg */}
        <motion.div
          transition={transitionSlow}
          variants={VFromBottom}
          initial="initial"
          whileInView="final"
          exit="initial"
          className="relative self-end mr-[-130px] mt-[-300px] md:mt-0 md:mr-5 lg:mr-0 max-w-[350px] max-h-[280px] md:max-w-full md:max-h-fit"
        >
          {/* bg */}
          <div
            className="absolute top-[200px] right-0 -z-10 w-[560px] h-[250px] rounded-3xl opacity-60
            gradient blur-3xl"
          ></div>
          {/* image */}
          <Image src={"/MainImg.png"} width={526} height={626} alt="" />
        </motion.div>
      </div>

      {/* benefits */}
      <motion.div
        variants={VFromBottom}
        initial="initial"
        whileInView="final"
        exit="initial"
        className="mx-5 lg:mx-0 mt-[137px] md:mt-0 px-8 lg:px-16 py-10 flex flex-col md:flex-row gap-12 bg-[--main-dark-color] border border-gray-500"
      >
        <div className="flex flex-col w-full h-full max-w-[324px]">
          <div className="w-[70px] h-[70px] flex justify-center items-center">
            <Image
              src={"/icons/mainIcons/1.svg"}
              width={70}
              height={70}
              alt="icon"
            />
          </div>
          <h3 className="pt-10 font-normal text-xl">Expertise</h3>
          <p className="pt-3 font-normal text-sm text-[--gray]">
            Our team consists of experienced VR developers, designers, and
            technicians who have a passion for VR and a commitment to delivering
            quality work and give the best service
          </p>
        </div>
        <div className="flex flex-col w-full h-full max-w-[324px]">
          <div className="w-[70px] h-[70px] flex justify-center items-center">
            <Image
              src={"/icons/mainIcons/2.svg"}
              width={70}
              height={70}
              alt="icon"
            />
          </div>
          <h3 className="pt-10 font-normal text-xl">Expertise</h3>
          <p className="pt-3 font-normal text-sm text-[--gray]">
            Our team consists of experienced VR developers, designers, and
            technicians who have a passion for VR and a commitment to delivering
            quality work and give the best service
          </p>
        </div>
        <div className="flex flex-col w-full h-full max-w-[324px]">
          <div className="w-[70px] h-[70px] flex justify-center items-center">
            <Image
              src={"/icons/mainIcons/3.svg"}
              width={70}
              height={70}
              alt="icon"
            />
          </div>
          <h3 className="pt-10 font-normal text-xl">Expertise</h3>
          <p className="pt-3 font-normal text-sm text-[--gray]">
            Our team consists of experienced VR developers, designers, and
            technicians who have a passion for VR and a commitment to delivering
            quality work and give the best service
          </p>
        </div>
      </motion.div>

      {/* bg */}
      <div
        className="after:bg-[url(/bg/striped.png)] after:content-[''] after:absolute after:top-0 after:right-0 after:w-full after:md:w-[730px] after:h-[350px] after:bg-no-repeat
        before:bg-[url(/bg/square.png)] before:content-[''] before:absolute before:top-[150px] before:left-0 before:bg-no-repeat before:w-full before:h-full before:-z-20"
      />
    </section>
  );
};

export default MainSection;
