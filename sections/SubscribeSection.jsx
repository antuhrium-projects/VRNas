"use client";

import { useState } from "react";
import { motion } from "framer-motion";

import {
  VFromBottom,
  VFromLeft,
  VFromRight,
  transitionFast,
  transitionSlow,
} from "@/variants";
import { TbMail } from "react-icons/tb";
import { TbSend } from "react-icons/tb";

const SubscribeSection = () => {
  const [value, setValue] = useState("");

  return (
    <section className="container px-5 mt-[100px] md:mt-[150px]">
      <div className="gradient w-full py-10 px-4 rounded-sm flex flex-col md:items-center md:flex-row md:justify-between gap-8 lg:px-16">
        <motion.h2
          variants={VFromLeft}
          initial="initial"
          whileInView="final"
          exit="initial"
          className="section-title text-center md:text-left md:max-w-[50%] text-3xl"
        >
          Bringing Your Virtual Reality Dreams to Life
        </motion.h2>
        <motion.form
          action=""
          variants={VFromRight}
          initial="initial"
          whileInView="final"
          exit="initial"
          className="flex items-center w-full bg-white bg-opacity-15 pl-4 pr-1 md:h-fit md:w-[365px]"
        >
          <TbMail className="text-3xl" />
          <input
            value={value}
            onChange={(e) => setValue(e.target.value)}
            type="text"
            placeholder="Enter your email address"
            className="bg-transparent placeholder:text-white py-4 pr-6 ml-2 w-full outline-none text-base "
          />
          <button type="submit" className="p-4 bg-white bg-opacity-40">
            <TbSend />
          </button>
        </motion.form>
      </div>
    </section>
  );
};

export default SubscribeSection;
