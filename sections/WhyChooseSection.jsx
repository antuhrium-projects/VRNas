"use client";

import { motion } from "framer-motion";

import DropText from "@/components/DropText";
import Image from "next/image";
import { VFromBottom, transitionFast, transitionSlow } from "@/variants";

const WhyChooseSection = () => {
  return (
    <section className="container px-5 mt-[100px] md:mt-[150px] overflow-x-hidden md:overflow-x-visible flex flex-col-reverse lg:flex-row items-center md:justify-between">
      {/* Left Text */}
      <motion.div
        variants={VFromBottom}
        initial="initial"
        whileInView="final"
        exit="initial"
        className="flex flex-col gap-10 w-full lg:max-w-[550px]"
      >
        <div className="flex flex-col gap-3 mt-20">
          <h4 className="section-name">WHY CHOOSE US</h4>
          <h2 className="max-w-[550px] section-title">
            Why Choose Us for Your VR Needs
          </h2>
        </div>

        <DropText />
        <DropText />
        <DropText />
      </motion.div>
      {/* Right Image */}
      <motion.div
        transition={transitionSlow}
        variants={VFromBottom}
        initial="initial"
        whileInView="final"
        exit="initial"
        className="relative lg:w-1/2 mr-[-100px]"
      >
        <motion.div
          transition={transitionFast}
          variants={VFromBottom}
          initial="initial"
          whileInView="final"
          exit="initial"
          className="w-[500px] h-[150px] lg:w-[600px] md:h-[200px] absolute bottom-[150px] left-[-60px] z-10 gradient blur-3xl opacity-60 rotate-[-45deg]"
        />
        <Image
          src={"/WhyChooseImage.png"}
          height={662}
          width={563}
          alt="WhyChooseImage"
        />
      </motion.div>
    </section>
  );
};

export default WhyChooseSection;
